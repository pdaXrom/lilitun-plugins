#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/sys_domain.h>
#include <sys/kern_control.h>
#include <net/if_utun.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <syslog.h>
#include <errno.h>
#include "main.h"

/**
 * @brief Allocate TUN device
 * @param ifName TUN interface name
 * @param flags TUN flags
 * @return Tun socket
 */
int AllocateTun(char **ifName, int flags)
{
	char device_fd = socket(PF_SYSTEM, SOCK_DGRAM, SYSPROTO_CONTROL);

	if(device_fd == -1) {
		syslog(LOG_ERR, "Could not open PF_SYSTEM socket: %s\n", strerror(errno));
		return device_fd;
	}

	struct ctl_info info = {};

	strlcpy(info.ctl_name, UTUN_CONTROL_NAME, sizeof(info.ctl_name));

	if(ioctl(device_fd, CTLIOCGINFO, &info) == -1) {
		syslog(LOG_ERR, "ioctl(CTLIOCGINFO) failed: %s", strerror(errno));
		return -1;
	}

	int unit = -1;
	char *p = strstr(*ifName, "utun"), *e = NULL;

	if(p) {
		unit = strtol(p + 4, &e, 10);

		if(!e) {
			unit = -1;
		}
	}

	struct sockaddr_ctl sc = {
		.sc_id = info.ctl_id,
		.sc_len = sizeof(sc),
		.sc_family = AF_SYSTEM,
		.ss_sysaddr = AF_SYS_CONTROL,
		.sc_unit = unit + 1,
	};

	if(connect(device_fd, (struct sockaddr *)&sc, sizeof(sc)) == -1) {
		syslog(LOG_ERR, "Could not connect utun socket: %s\n", strerror(errno));
		return -1;
	}

	char name[64] = "";
	socklen_t len = sizeof(name);

	if(getsockopt(device_fd, SYSPROTO_CONTROL, UTUN_OPT_IFNAME, name, &len)) {
	} else {
		free(*ifName);
		*ifName = strdup(name);
	}

	if (debug) {
	    syslog(LOG_INFO, "%s: Allocated device %s", __func__, *ifName);
	}

	return device_fd;
}

/**
  * @brief Close TUN device
  * @param fd Socket
  */
int CloseTun(int fd)
{
    return close(fd);
}

/**
  * @brief Create TUN device
  * @param ifName TUN interface name
  * @param ip IP
  * @param netmask Netmask
  * @flags Flags
  * @return Status
  */
int CreateTun(char **ifName, struct in_addr ip, struct in_addr netmask, int flags)
{
    return 0;
}

/**
  * @brief Delete TUN device
  * @param ifName TUN interface name
  * @flags Flags
  * @return Status
  */
int DeleteTun(char **ifName, int flags)
{
    return 0;
}

//remove the IP version header from the result of bytes read or written.
static inline ssize_t header_modify_read_write_return (ssize_t len)
{
    if (len > 0) {
	return len > (ssize_t) sizeof(u_int32_t) ? len - sizeof(u_int32_t) : 0;
    }

    return len;
}

/**
 * @brief Read from UTUN
 * @param fd Socket
 * @param buf Buffer
 * @param len Length
 * @return Length
 */
int ReadTun(int fd, char *buf, int len)
{
    u_int32_t type;
    struct iovec iv[2];
    struct ip *iph;
    
    iph = (struct ip *) buf;

    if(iph->ip_v == 6)
	type = htonl(AF_INET6);
    else
	type = htonl(AF_INET);

    iv[0].iov_base = (char *)&type;
    iv[0].iov_len = sizeof (type);
    iv[1].iov_base = buf;
    iv[1].iov_len = len;

    return header_modify_read_write_return(readv(fd, iv, 2));
}

/**
 * @brief Write from UTUN
 * @param fd Socket
 * @param buffer Buffer
 * @param len Length
 * @return Length
 */
int WriteTun(int fd, char *buf, int len)
{
    u_int32_t type;
    struct iovec iv[2];
    struct ip *iph;

    iph = (struct ip *) buf;

    if(iph->ip_v == 6)
	type = htonl(AF_INET6);
    else
	type = htonl(AF_INET);

    iv[0].iov_base = (char *)&type;
    iv[0].iov_len  = sizeof (type);
    iv[1].iov_base = buf;
    iv[1].iov_len  = len;

    return header_modify_read_write_return(writev(fd, iv, 2));
}
