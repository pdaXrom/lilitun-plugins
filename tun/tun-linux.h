/**
 * @file tun-linux.h
 * @brief Functions for work with TUN interface
 */
#ifndef __TUN_LINUX_H__
#define __TUN_LINUX_H__

#if defined(__linux__)
#define ReadTun read
#define WriteTun write
#else
int ReadTun(int fd, char *buf, int len);
int WriteTun(int fd, char *buf, int len);
#endif

int CreateTun(char **ifName, struct in_addr ip, struct in_addr netmask, int flags);
int DeleteTun(char **ifName, int flags);
int AllocateTun(char **ifName, int flags);
int CloseTun(int fd);

#ifdef __BIONIC__
int ConfigureVpnService(struct in_addr ip, struct in_addr mask, struct in_addr dns1, struct in_addr dns2, time_t expiry, uint32_t version);
void RemoveVpnService();
void ProtectFd(int fd);
#endif

#ifdef _WIN32
int SetupTunGatewayAndDns(char *ifName, struct in_addr gateway, struct in_addr dns[4]);
int SetupTunStatus(char *ifName, int val);
#endif

#endif
