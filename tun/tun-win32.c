#include <windows.h>
#include <winioctl.h>
#include <syslog.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/types.h>

//=============
// TAP IOCTLs
//=============

#define TAP_CONTROL_CODE(request,method) \
  CTL_CODE (FILE_DEVICE_UNKNOWN, request, method, FILE_ANY_ACCESS)

#define TAP_IOCTL_GET_MAC               TAP_CONTROL_CODE (1, METHOD_BUFFERED)
#define TAP_IOCTL_GET_VERSION           TAP_CONTROL_CODE (2, METHOD_BUFFERED)
#define TAP_IOCTL_GET_MTU               TAP_CONTROL_CODE (3, METHOD_BUFFERED)
#define TAP_IOCTL_GET_INFO              TAP_CONTROL_CODE (4, METHOD_BUFFERED)
#define TAP_IOCTL_CONFIG_POINT_TO_POINT TAP_CONTROL_CODE (5, METHOD_BUFFERED)
#define TAP_IOCTL_SET_MEDIA_STATUS      TAP_CONTROL_CODE (6, METHOD_BUFFERED)
#define TAP_IOCTL_CONFIG_DHCP_MASQ      TAP_CONTROL_CODE (7, METHOD_BUFFERED)
#define TAP_IOCTL_GET_LOG_LINE          TAP_CONTROL_CODE (8, METHOD_BUFFERED)
#define TAP_IOCTL_CONFIG_DHCP_SET_OPT   TAP_CONTROL_CODE (9, METHOD_BUFFERED)

/* obsoletes TAP_IOCTL_CONFIG_POINT_TO_POINT */
#define TAP_IOCTL_CONFIG_TUN            TAP_CONTROL_CODE (10, METHOD_BUFFERED)

//=================
// Registry keys
//=================

#define ADAPTER_KEY "SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}"

#define NETWORK_CONNECTIONS_KEY "SYSTEM\\CurrentControlSet\\Control\\Network\\{4D36E972-E325-11CE-BFC1-08002BE10318}"

//======================
// Filesystem prefixes
//======================

#define USERMODEDEVICEDIR "\\\\.\\Global\\"
#define TAPSUFFIX         ".tap"

static const char *TAP_COMPONENT_ID[] = { "tap0901", "tap0801", NULL };

typedef enum {
    SEARCH_IF_GUID_FROM_NAME,
    SEARCH_IF_NAME_FROM_GUID
} search_if_en;

typedef uint32_t in_addr_t;

static OVERLAPPED overlap_read, overlap_write;

static char *search_if(char *value, char *key, search_if_en type)
{
    int i = 0;
    LONG status;
    DWORD len;
    HKEY net_conn_key;
    BOOL found = FALSE;
    char guid[256];
    char ifname[256];
    char conn_string[512];
    HKEY conn_key;
    DWORD value_type;

    if (!value || !key) {
	return NULL;
    }

    status = RegOpenKeyEx(HKEY_LOCAL_MACHINE, NETWORK_CONNECTIONS_KEY, 0, KEY_READ, &net_conn_key);

    if (status != ERROR_SUCCESS) {
	syslog(LOG_ERR, "Error opening registry key: %s", NETWORK_CONNECTIONS_KEY);
	return NULL;
    }

    while (!found) {
	len = sizeof(guid);
	status = RegEnumKeyEx(net_conn_key, i++, guid, &len, NULL, NULL, NULL, NULL);
	if (status == ERROR_NO_MORE_ITEMS) {
	    break;
	} else if (status != ERROR_SUCCESS) {
	    continue;
	}
	snprintf(conn_string, sizeof(conn_string), "%s\\%s\\Connection", NETWORK_CONNECTIONS_KEY, guid);
	status = RegOpenKeyEx(HKEY_LOCAL_MACHINE, conn_string, 0, KEY_READ, &conn_key);
	if (status != ERROR_SUCCESS) {
	    continue;
	}
	len = sizeof(ifname);
	status = RegQueryValueEx(conn_key, "Name", NULL, &value_type, (LPBYTE) ifname, &len);
	if (status != ERROR_SUCCESS || value_type != REG_SZ) {
	    RegCloseKey(conn_key);
	    continue;
	}

	switch (type) {
	case SEARCH_IF_GUID_FROM_NAME:
	    if (!strcmp(key, ifname)) {
		strcpy(value, guid);
		found = TRUE;
	    }
	    break;
	case SEARCH_IF_NAME_FROM_GUID:
	    if (!strcmp(key, guid)) {
		strcpy(value, ifname);
		found = TRUE;
	    }
	    break;
	default:
	    break;
	}
	RegCloseKey(conn_key);
    }
    RegCloseKey(net_conn_key);

    if (found) {
	return value;
    }

    return NULL;
}

/*
 * Open the TUN/TAP device with the provided guid
 */
static HANDLE open_tun_device(char *guid, char *dev)
{
    HANDLE handle;
    ULONG len, info[3];
    char device_path[512];

    syslog(LOG_INFO, "Device: %s", dev);

    /*
     * Let's try to open Windows TAP-Win32 adapter
     */
    snprintf(device_path, sizeof(device_path), "%s%s%s", USERMODEDEVICEDIR, guid, TAPSUFFIX);

    handle = CreateFile(device_path, GENERIC_READ | GENERIC_WRITE, 0,	/* Don't let other processes share or open
									   the resource until the handle's been closed */
			0, OPEN_EXISTING, FILE_ATTRIBUTE_SYSTEM | FILE_FLAG_OVERLAPPED, 0);

    if (handle == INVALID_HANDLE_VALUE) {
	syslog(LOG_ERR, "%s: Invalid handle", __func__);
	return NULL;
    }

    /*
     * get driver version info
     */
    memset(info, 0, sizeof(info));
    if (DeviceIoControl(handle, TAP_IOCTL_GET_VERSION, &info, sizeof(info), &info, sizeof(info), &len, NULL)) {
	syslog(LOG_INFO, "TAP-Win32 Driver Version %d.%d %s", (int)info[0], (int)info[1], (info[2] ? "(DEBUG)" : ""));
    }

    /*
     * Set driver media status to 'connected'
     */
//      status = TRUE;
//      if (!DeviceIoControl(handle, TAP_IOCTL_SET_MEDIA_STATUS,
//              &status, sizeof(status),
//              &status, sizeof(status), &len, NULL)) {
//              printf("WARNING: The TAP-Win32 driver rejected a "
//              "TAP_IOCTL_SET_MEDIA_STATUS DeviceIoControl call.\n");
//      }

    /*
     * Initialize overlapped structures
     */
    overlap_read.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    overlap_write.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (!overlap_read.hEvent || !overlap_write.hEvent) {
	syslog(LOG_ERR, "%s: Cannot create overlap structures.", __func__);
	return NULL;
    }

    /*
     * Return fd
     */
//      return cygwin_attach_handle_to_fd(NULL, -1, handle, 1, GENERIC_READ | GENERIC_WRITE);

    return handle;
}

/*
 * Allocate TUN device, returns opened fd.
 * Stores dev name in the first arg (must be large enough).
 */
static HANDLE tun_open(char **dev)
{
    HANDLE fd = NULL;
    HKEY unit_key;
    char guid[256];
    char comp_id[256];
    char enum_name[256];
    char unit_string[512];
    BOOL found = FALSE;
    HKEY adapter_key;
    DWORD value_type;
    LONG status;
    DWORD len;

    /*
     * Device name has been provided. Open such device.
     */
    if (*dev && **dev != '\0') {
	if (!search_if(guid, *dev, SEARCH_IF_GUID_FROM_NAME)) {
	    syslog(LOG_INFO, "Device %s not found.", *dev);
//	    return NULL;
	} else {
	    return open_tun_device(guid, *dev);
	}
    }

    if (*dev) {
	*dev = realloc(*dev, PATH_MAX);
    }

    /*
     * Device name has non been specified. Look for one available!
     */
    int i = 0;
    status = RegOpenKeyEx(HKEY_LOCAL_MACHINE, ADAPTER_KEY, 0, KEY_READ, &adapter_key);
    if (status != ERROR_SUCCESS) {
	syslog(LOG_ERR, "Error opening registry key: %s", ADAPTER_KEY);
	return NULL;
    }

    while (!found) {
	len = sizeof(enum_name);
	status = RegEnumKeyEx(adapter_key, i++, enum_name, &len, NULL, NULL, NULL, NULL);
	if (status == ERROR_NO_MORE_ITEMS) {
	    break;
	} else if (status != ERROR_SUCCESS) {
	    continue;
	}
	snprintf(unit_string, sizeof(unit_string), "%s\\%s", ADAPTER_KEY, enum_name);
	status = RegOpenKeyEx(HKEY_LOCAL_MACHINE, unit_string, 0, KEY_READ, &unit_key);
	if (status != ERROR_SUCCESS) {
	    continue;
	}
	len = sizeof(comp_id);
	status = RegQueryValueEx(unit_key, "ComponentId", NULL, &value_type, (LPBYTE) comp_id, &len);
	if (status != ERROR_SUCCESS || value_type != REG_SZ) {
	    RegCloseKey(unit_key);
	    continue;
	}
	len = sizeof(guid);
	status = RegQueryValueEx(unit_key, "NetCfgInstanceId", NULL, &value_type, (LPBYTE) guid, &len);
	if (status != ERROR_SUCCESS || value_type != REG_SZ) {
	    RegCloseKey(unit_key);
	    continue;
	}

	int j = 0;
	while (TAP_COMPONENT_ID[j]) {
	    if (!strcmp(comp_id, TAP_COMPONENT_ID[j])) {
		break;
	    }
	    j++;
	}
	if (!TAP_COMPONENT_ID[j]) {
	    RegCloseKey(unit_key);
	    continue;
	}

	/*
	 * Let's try to open this device
	 */
	search_if(*dev, guid, SEARCH_IF_NAME_FROM_GUID);
	fd = open_tun_device(guid, *dev);
	if (fd) {
	    found = TRUE;
	}

	RegCloseKey(unit_key);
    }
    RegCloseKey(adapter_key);

    return fd;
}

int CreateTun(char **ifName, struct in_addr ip, struct in_addr netmask, int flags)
{
    return 0;
}

int DeleteTun(char **ifName, int flags)
{
    return 0;
}

static struct _tunFdItem {
    HANDLE handle;
    char *dev;
} tunFdItem[256];

static int tunFd = -1;

int AllocateTun(char **ifName, int flags)
{
    int fd;

    if (tunFd == -1) {
	memset(&tunFdItem, 0, sizeof(tunFdItem));
	tunFd = 0;
    }

    for (fd = 0; fd < sizeof(tunFdItem) / sizeof(struct _tunFdItem); fd++) {
	if (tunFdItem[fd].handle == NULL) {
	    tunFdItem[tunFd].handle = tun_open(ifName);
	    if (tunFdItem[tunFd].handle) {
		tunFdItem[tunFd].dev = *ifName;
		return fd;
	    }
	}
    }

    return -1;
}

/**
  * @brief Close TUN device
  * @param fd Socket
  */
int CloseTun(int fd)
{
    int ret = CloseHandle(tunFdItem[fd].handle);
    tunFdItem[fd].handle = NULL;
    tunFdItem[fd].dev = NULL;
    return ret;
}

int ReadTun(int fd, char *buf, int len)
{
    DWORD read_size;

    ResetEvent(overlap_read.hEvent);
    if (ReadFile(tunFdItem[fd].handle, buf, len, &read_size, &overlap_read)) {
	return read_size;
    }
    switch (GetLastError()) {
    case ERROR_IO_PENDING:
	WaitForSingleObject(overlap_read.hEvent, INFINITE);
	GetOverlappedResult(tunFdItem[fd].handle, &overlap_read, &read_size, FALSE);
	return read_size;
	break;
    default:
	break;
    }

    return -1;
}

int WriteTun(int fd, char *buf, int len)
{
    DWORD write_size;

    ResetEvent(overlap_write.hEvent);
    if (WriteFile(tunFdItem[fd].handle, buf, len, &write_size, &overlap_write)) {
	return write_size;
    }
    switch (GetLastError()) {
    case ERROR_IO_PENDING:
	WaitForSingleObject(overlap_write.hEvent, INFINITE);
	GetOverlappedResult(tunFdItem[fd].handle, &overlap_write, &write_size, FALSE);
	return write_size;
	break;
    default:
	break;
    }

    return -1;
}

int SetInterfaceAddress(char *ifName, struct in_addr ip, struct in_addr netmask, struct in_addr dstip)
{
    int fd;
//    ULONG status;
    DWORD len;
    HANDLE handle = NULL;

    for (fd = 0; fd < sizeof(tunFdItem) / sizeof(struct _tunFdItem); fd++) {
	if (!strcmp(tunFdItem[fd].dev, ifName)) {
	    handle = tunFdItem[fd].handle;
	    break;
	}
    }

    if (!handle) {
	syslog(LOG_ERR, "%s: Cannot find handle", __func__);
	return -1;
    }

//    long addrs[3]= { ip.s_addr,
//                   ip.s_addr & netmask.s_addr,
//                   netmask.s_addr };
    in_addr_t ep[3];
    ep[0] = ip.s_addr;
    ep[1] = ip.s_addr & netmask.s_addr;
    ep[2] = netmask.s_addr;

//    if (!DeviceIoControl(handle, TAP_IOCTL_CONFIG_POINT_TO_POINT, ep, sizeof(ep), ep, sizeof(ep), &len, NULL)) {
    if (!DeviceIoControl(handle, TAP_IOCTL_CONFIG_TUN, ep, sizeof(ep), ep, sizeof(ep), &len, NULL)) {
	syslog(LOG_ERR, "%s: point-to-point configuration refused", __func__);
	return -1;
    }
//    }

    {
	uint32_t ep[4];
	ep[0] = ip.s_addr;
	ep[1] = netmask.s_addr;
	ep[2] = dstip.s_addr;
	ep[3] = 1234567;

	if (!DeviceIoControl(handle, TAP_IOCTL_CONFIG_DHCP_MASQ, ep, sizeof(ep), ep, sizeof(ep), &len, NULL)) {
	    syslog(LOG_ERR, "%s: dhcp masq configuration refused", __func__);
	    return -1;
	}
    }

//    status = TRUE;
//    if (!DeviceIoControl(handle, TAP_IOCTL_SET_MEDIA_STATUS, &status, sizeof(status), &status, sizeof(status), &len, NULL)) {
//	syslog(LOG_WARNING, "WARNING: The TAP-Win32 driver rejected a " "TAP_IOCTL_SET_MEDIA_STATUS DeviceIoControl call.");
//    }

    return 0;
}

int SetupTunGatewayAndDns(char *ifName, struct in_addr gateway, struct in_addr dns[4])
{
    int i;
    int fd;
    char buf[256];
    int bufLen;
    struct in_addr *addr;
    DWORD len;
    HANDLE handle = NULL;

    for (fd = 0; fd < sizeof(tunFdItem) / sizeof(struct _tunFdItem); fd++) {
	if (!strcmp(tunFdItem[fd].dev, ifName)) {
	    handle = tunFdItem[fd].handle;
	    break;
	}
    }

    if (!handle) {
	syslog(LOG_ERR, "%s: Cannot find handle", __func__);
	return -1;
    }

    buf[0] = 3;
    buf[1] = 4;
    addr = (struct in_addr *)&buf[2];
    addr[0].s_addr = gateway.s_addr;

    buf[6] = 6;

    addr = (struct in_addr *)&buf[8];
    for (i = 0; i < 4; i++) {
	if (dns[i].s_addr == 0xFFFFFFFF) {
	    break;
	}
	addr[i].s_addr = dns[i].s_addr;
    }

    buf[7] = i * 4;

    bufLen = 6 + 2 + i * 4;

    if (!DeviceIoControl(handle, TAP_IOCTL_CONFIG_DHCP_SET_OPT, buf, bufLen, buf, bufLen, &len, NULL)) {
	syslog(LOG_ERR, "WARNING: The TAP-Win32 driver rejected a TAP_IOCTL_CONFIG_DHCP_SET_OPT DeviceIoControl call.");
	return -1;
    }

    return 0;
}

int SetupTunStatus(char *ifName, int val)
{
    int fd;
    DWORD len;
    HANDLE handle = NULL;
    ULONG status = val;

    for (fd = 0; fd < sizeof(tunFdItem) / sizeof(struct _tunFdItem); fd++) {
	if (!strcmp(tunFdItem[fd].dev, ifName)) {
	    handle = tunFdItem[fd].handle;
	    break;
	}
    }

    if (!handle) {
	syslog(LOG_ERR, "%s: Cannot find handle", __func__);
	return -1;
    }

    if (!DeviceIoControl(handle, TAP_IOCTL_SET_MEDIA_STATUS, &status, sizeof(status), &status, sizeof(status), &len, NULL)) {
	syslog(LOG_ERR, "WARNING: The TAP-Win32 driver rejected a " "TAP_IOCTL_SET_MEDIA_STATUS DeviceIoControl call.");
	return -1;
    }
    return 0;
}
