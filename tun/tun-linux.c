#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <linux/if_tun.h>
#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdlib.h>
#include <syslog.h>
#include <stddef.h>
#include <pthread.h>
#include "tun-linux.h"

/**
 * @brief Allocate TUN device
 * @param ifName TUN interface name
 * @param flags TUN flags
 * @return Tun socket
 */
int AllocateTun(char **ifName, int flags)
{

    struct ifreq ifr;
    int fd, err;
    char *clonedev = "/dev/net/tun";

    if ((fd = open(clonedev, O_RDWR)) < 0) {
	syslog(LOG_ERR, "%s: Opening /dev/net/tun (%s)", __func__, strerror(errno));
	return fd;
    }

    memset(&ifr, 0, sizeof(ifr));

    ifr.ifr_flags = flags | IFF_TUN;

    if (*ifName) {
	strncpy(ifr.ifr_name, *ifName, IFNAMSIZ);
    }

    if ((err = ioctl(fd, TUNSETIFF, (void *)&ifr)) < 0) {
	syslog(LOG_ERR, "%s: Error ioctl (%s)", __func__, strerror(errno));
	close(fd);
	return err;
    }

    return fd;
}

/**
  * @brief Close TUN device
  * @param fd Socket
  */
int CloseTun(int fd)
{
    return close(fd);
}

/**
  * @brief Create TUN device
  * @param ifName TUN interface name
  * @param ip IP
  * @param netmask Netmask
  * @flags Flags
  * @return Status
  */
int CreateTun(char **ifName, struct in_addr ip, struct in_addr netmask, int flags)
{
    int fd = AllocateTun(ifName, flags);
    if (fd != -1) {
	if (ioctl(fd, TUNSETPERSIST, 1) != -1) {
	    close(fd);
	    return 0;
	} else {
	    close(fd);
	}
    }

    syslog(LOG_ERR, "%s: Error (%s)", __func__, strerror(errno));

    return -1;
}

/**
  * @brief Delete TUN device
  * @param ifName TUN interface name
  * @flags Flags
  * @return Status
  */
int DeleteTun(char **ifName, int flags)
{
    int fd = AllocateTun(ifName, flags);
    if (fd != -1) {
	if (ioctl(fd, TUNSETPERSIST, 0) != -1) {
	    close(fd);
	    return 0;
	}
	close(fd);
    }

    syslog(LOG_ERR, "%s: Error (%s)", __func__, strerror(errno));

    return -1;
}
