#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <net/if.h>
#include <net/route.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <netdb.h>
#include <resolv.h>
#ifdef __linux__
#include <linux/if_tun.h>
#include <linux/rtnetlink.h>
#include <linux/ipv6.h>
#endif
#ifdef __APPLE__
#include <sys/param.h>
#include <sys/sysctl.h>
#endif
#endif

#include "plugin.h"
#include "tun-linux.h"

#ifndef _WIN32
#define closesocket close
#endif

#define CLIENT_CONFIGURATION_ERROR	7

#define BUFSIZE	2048

static const char *Name = "TUN";
static const int Version = (0.1 * 1000);

static LiliTunPlugin *plugin;
static void *connection;

static struct in_addr vpnIp;
static struct in_addr vpnNetmask;
static struct in_addr vpnGateway;
static struct in_addr vpnDns[4];
static struct in_addr remoteIp;
static struct in_addr hostGateway;
static char hostGatewayIfName[32];

static int serverHostRoute;

static char *tunIfName;
static int tunFd;

static int isAlive;

#ifdef _WIN32
BOOL IsCurrentUserLocalAdministrator();
int SetInterfaceAddress(char *ifName, struct in_addr ip, struct in_addr netmask, struct in_addr dstip);
#else
int    IsCurrentUserLocalAdministrator();
static int SetInterfaceAddress(char *ifName, struct in_addr ip, struct in_addr netmask, struct in_addr dstip);
static int GetNetworkGateway(struct in_addr *ip, char *ifName, int ifNameLen);
#endif
static int SetNetworkRoute(int op, struct in_addr ip, struct in_addr netmask, struct in_addr gateway, char *ifName, int flags);
static int SetDefaultGateway(int op, struct in_addr gateway, char *ifName);
static int SetupInterfaceAndRouting(char *tunIfName, struct in_addr vpnIp, struct in_addr vpnNetmask, struct in_addr vpnGateway, struct in_addr vpnDns[4], struct in_addr remoteIp);
static int RemoveInterfaceAndRouting(struct in_addr remoteIp);

static void *TunToNetThread(void *arg);
static void *NetToTunThread(void *arg);

STATIC_PLUGIN_FUNCTION int SetupAddress(void *connection, struct in_addr ip, struct in_addr nm)
{
    vpnIp = ip;
    vpnNetmask = nm;
    return 0;
}

STATIC_PLUGIN_FUNCTION int SetupGateway(void *connection, struct in_addr gw)
{
    vpnGateway = gw;
    return 0;
}

STATIC_PLUGIN_FUNCTION int SetupDns(void *connection, struct in_addr dns[4])
{
    int i;
    for (i = 0; i < 4; i++) {
	vpnDns[i] = dns[i];
    }
    return 0;
}

STATIC_PLUGIN_FUNCTION int MainLoop(void *_connection, struct in_addr _remoteIp)
{
    int status;
    pthread_t net2tap_tid, tap2net_tid;
    int tunFlags;
    serverHostRoute = 0;
    remoteIp = _remoteIp;

    connection = _connection;


#if defined(__linux__)
    tunIfName = strdup("lil0");
    tunFlags = IFF_NO_PI;
#elif defined(_WIN32)
    tunIfName = strdup("Ethernet TAP");
    tunFlags = 0;
#else
    tunIfName = strdup("lil0");
    tunFlags = 0;
#endif

    /* initialize tun/tap interface */
    if ((tunFd = AllocateTun(&tunIfName, tunFlags)) < 0) {
//	conn->status = CLIENT_CONFIGURATION_ERROR;
	syslog(LOG_ERR, "%s: Error connecting to tun/tap interface %s (%s)", __func__, tunIfName, strerror(errno));
	return -1;
    }

    SetupInterfaceAndRouting(tunIfName, vpnIp, vpnNetmask, vpnGateway, vpnDns, remoteIp);


    isAlive = 1;

    if (pthread_create(&net2tap_tid, NULL, &NetToTunThread, NULL) != 0) {
	syslog(LOG_ERR, "%s: pthread_create(NetToTunThread) (%s)", __func__, strerror(errno));
	status = CLIENT_CONFIGURATION_ERROR;
    } else if (pthread_create(&tap2net_tid, NULL, &TunToNetThread, NULL) != 0) {
	syslog(LOG_ERR, "%s: pthread_create(TunToNetThread) (%s)", __func__, strerror(errno));
	status = CLIENT_CONFIGURATION_ERROR;
    } else {
	syslog(LOG_INFO, "VPN connection started");

	(void)pthread_join(net2tap_tid, NULL);
	(void)pthread_join(tap2net_tid, NULL);

	net2tap_tid = 0;
	tap2net_tid = 0;
    }

    syslog(LOG_INFO, "VPN connection finished");

    isAlive = 0;

    RemoveInterfaceAndRouting(remoteIp);

    CloseTun(tunFd);

    free(tunIfName);

    return status;
}

STATIC_PLUGIN_FUNCTION int Exit(void *connection)
{
    return -1;
}

int PluginInit(LiliTunPlugin *plug)
{
    plugin = plug;
    plug->Name = Name;
    plug->Version = &Version;
    plug->SetupAddress = (int (*)(void *connection, struct in_addr ip, struct in_addr nm)) SetupAddress;
    plug->SetupGateway = (int (*)(void *connection, struct in_addr gw)) SetupGateway;
    plug->SetupDns = (int (*)(void *connection, struct in_addr dns[4])) SetupDns;
    plug->MainLoop = (int (*)(void *connection, struct in_addr remoteIp)) MainLoop;
    plug->Exit = (int (*)(void *connection)) Exit;
    return 0;
}

/**
 * @brief Tun to network loop thread
 * @param arg Connection
 */
static void *TunToNetThread(void *arg)
{
    //ConnectionArg *conn = (ConnectionArg *) arg;

    char buffer[BUFSIZE];

    while (plugin->IsAlive(connection) && isAlive) {
/*
 * FIXME: timeout for Win32 tun read
 */
#ifndef _WIN32
	int ret;
	fd_set rfds;
	struct timeval tv;

	tv.tv_sec = 5; //conn->pingTime;
	tv.tv_usec = 0;

	FD_ZERO(&rfds);
	FD_SET(tunFd, &rfds);

	ret = select(tunFd + 1, &rfds, NULL, NULL, &tv);

	if (ret < 0 && errno == EINTR) {
	    continue;
	}

	if (ret < 0) {
	    syslog(LOG_ERR, "%s: Select error (%s)", __func__, strerror(errno));
	    break;
	}

	if (!ret) {
	    syslog(LOG_DEBUG, "%s: Select tap_fd timeout, continue", __func__);
	    continue;
	}

	if (FD_ISSET(tunFd, &rfds)) {
#endif
	    int nread = ReadTun(tunFd, buffer, sizeof(buffer) - 16);
	    if (nread <= 0) {
		syslog(LOG_ERR, "%s: Error read from tun (%s)", __func__, strerror(errno));
		break;
	    }

	    int len = plugin->EthernetPacketWrite(connection, buffer, nread);
	    if (len < 0) {
		syslog(LOG_ERR, "%s: Error write ethernet packet (%s)", __func__, strerror(errno));
		break;
	    }
/*
 * FIXME: timeout for Win32 tun read
 */
#ifndef _WIN32
	}
#endif
    }

    isAlive = 0;

    return NULL;
}

/**
 * @brief Net to tun loop thread
 * @param arg Connection
 */
static void *NetToTunThread(void *arg)
{
    char buffer[BUFSIZE];

    while (plugin->IsAlive(connection) && isAlive) {
	int ret;
	int delay = 500000;

	if ((ret = plugin->EthernetPacketAvailable(connection, delay)) > 0) {
	    int len = plugin->EthernetPacketRead(connection, buffer, sizeof(buffer));
	    if (len < 0) {
		syslog(LOG_ERR, "%s: Error read ethernet packet (%s)", __func__, strerror(errno));
		break;
	    }

	    if (len > 0) {
		int nwrite = WriteTun(tunFd, buffer, len);

		if (nwrite != len) {
		    syslog(LOG_ERR, "%s: error write buffer (%s)", __func__, strerror(errno));
		    break;
		}
	    }
	} else {
	    if (ret < 0 && errno != EINTR) {
		syslog(LOG_ERR, "%s: Select error (%s)", __func__, strerror(errno));
		break;
	    }
	}
    }

    isAlive = 0;

    return NULL;
}



#ifdef _WIN32
#define RTF_HOST	0
#endif

enum {
    ROUTE_DEL = 0,
    ROUTE_ADD = 1
};

#ifdef __linux__

struct route_info {
    struct in_addr dstAddr;
    struct in_addr srcAddr;
    struct in_addr gateWay;
    char ifName[IF_NAMESIZE];
};

/**
 * @brief Read netlink socket
 * @param fd Socket
 * @param bufPtr
 * @param bufSize
 * @param seqNum
 * @param pId
 * @return Status
 */
static int readNlSock(int sockFd, char *bufPtr, size_t buf_size, int seqNum, int pId)
{
    struct nlmsghdr *nlHdr;
    int readLen = 0, msgLen = 0;

    do {
	/* Recieve response from the kernel */
	if ((readLen = recv(sockFd, bufPtr, buf_size - msgLen, 0)) < 0) {
	    perror("SOCK READ: ");
	    return -1;
	}

	nlHdr = (struct nlmsghdr *)bufPtr;

	/* Check if the header is valid */
	if ((NLMSG_OK(nlHdr, readLen) == 0) || (nlHdr->nlmsg_type == NLMSG_ERROR)) {
	    perror("Error in recieved packet");
	    return -1;
	}

	/* Check if the its the last message */
	if (nlHdr->nlmsg_type == NLMSG_DONE) {
	    break;
	} else {
	    /* Else move the pointer to buffer appropriately */
	    bufPtr += readLen;
	    msgLen += readLen;
	}

	/* Check if its a multi part message */
	if ((nlHdr->nlmsg_flags & NLM_F_MULTI) == 0) {
	    /* return if its not */
	    break;
	}
    }
    while ((nlHdr->nlmsg_seq != seqNum) || (nlHdr->nlmsg_pid != pId));

    return msgLen;
}

/**
 * @brief Parse the route info returned
 * @param nlHdr
 * @param rtInfo
 * @return Status
 */
static int parseRoutes(struct nlmsghdr *nlHdr, struct route_info *rtInfo)
{
    struct rtmsg *rtMsg;
    struct rtattr *rtAttr;
    int rtLen;

    rtMsg = (struct rtmsg *)NLMSG_DATA(nlHdr);

    /* If the route is not for AF_INET or does not belong to main routing table then return. */
    if ((rtMsg->rtm_family != AF_INET) || (rtMsg->rtm_table != RT_TABLE_MAIN))
	return -1;

    /* get the rtattr field */
    rtAttr = (struct rtattr *)RTM_RTA(rtMsg);
    rtLen = RTM_PAYLOAD(nlHdr);

    for (; RTA_OK(rtAttr, rtLen); rtAttr = RTA_NEXT(rtAttr, rtLen)) {
	switch (rtAttr->rta_type) {
	case RTA_OIF:
	    if_indextoname(*(int *)RTA_DATA(rtAttr), rtInfo->ifName);
	    break;

	case RTA_GATEWAY:
	    memcpy(&rtInfo->gateWay, RTA_DATA(rtAttr), sizeof(rtInfo->gateWay));
	    break;

	case RTA_PREFSRC:
	    memcpy(&rtInfo->srcAddr, RTA_DATA(rtAttr), sizeof(rtInfo->srcAddr));
	    break;

	case RTA_DST:
	    memcpy(&rtInfo->dstAddr, RTA_DATA(rtAttr), sizeof(rtInfo->dstAddr));
	    break;
	}
    }

    return 0;
}

/**
 * @brief Find default network gateway
 * @param ip IP
 * @param ifName Interface name buffer
 * @param len Interface name buffer length
 * @return Status
 */
static int GetNetworkGateway(struct in_addr *ip, char *ifName, int ifNameLen)
{
    int found_gatewayip = -1;

    struct nlmsghdr *nlMsg;
//    struct rtmsg *rtMsg;
    struct route_info route_info;
    char msgBuf[8192];	// pretty large buffer

    int sock, len, msgSeq = 0;

    /* Create Socket */
    if ((sock = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE)) < 0) {
	perror("Socket Creation: ");
	return (-1);
    }

    /* Initialize the buffer */
    memset(msgBuf, 0, sizeof(msgBuf));

    /* point the header and the msg structure pointers into the buffer */
    nlMsg = (struct nlmsghdr *)msgBuf;
//    rtMsg = (struct rtmsg *)NLMSG_DATA(nlMsg);

    /* Fill in the nlmsg header */
    nlMsg->nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg));	// Length of message.
    nlMsg->nlmsg_type = RTM_GETROUTE;	// Get the routes from kernel routing table .

    nlMsg->nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST;	// The message is a request for dump.
    nlMsg->nlmsg_seq = msgSeq++;	// Sequence of the message packet.
    nlMsg->nlmsg_pid = getpid();	// PID of process sending the request.

    /* Send the request */
    if (send(sock, nlMsg, nlMsg->nlmsg_len, 0) < 0) {
	syslog(LOG_ERR, "%s: Write to socket failed (%s)", __func__, strerror(errno));
	return -1;
    }

    /* Read the response */
    if ((len = readNlSock(sock, msgBuf, sizeof(msgBuf), msgSeq, getpid())) < 0) {
	syslog(LOG_ERR, "%s: read from socket failed (%s)", __func__, strerror(errno));
	return -1;
    }

    /* Parse and print the response */
    for (; NLMSG_OK(nlMsg, len); nlMsg = NLMSG_NEXT(nlMsg, len)) {
	memset(&route_info, 0, sizeof(route_info));
	if (parseRoutes(nlMsg, &route_info) < 0)
	    continue;		// don't check route_info if it has not been set up

	// Check if default gateway
	if (route_info.dstAddr.s_addr == 0) {
	    // copy it over
	    //inet_ntop(AF_INET, &route_info.gateWay, gatewayip, size);
	    ip->s_addr = route_info.gateWay.s_addr;
	    if (ifName) {
		strncpy(ifName, route_info.ifName, ifNameLen);
	    }
	    found_gatewayip = 0;
	    break;
	}
    }

    closesocket(sock);

    return found_gatewayip;
}

/**
 * @brief Set network route
 * @param op Operation
 * @param ifName Interface name
 * @param ip Destination IP
 * @param netmasn Destination mask
 * @param gateway Gateway
 * @return Status
 */
static int SetNetworkRoute(int op, struct in_addr ip, struct in_addr netmask, struct in_addr gateway, char *ifName, int flags)
{
    int r;
    struct rtentry route;
    struct sockaddr_in *addr;
    int fd = socket( PF_INET, SOCK_DGRAM, IPPROTO_IP );

    memset( &route, 0, sizeof( route ) );

    route.rt_dev = ifName;

    addr = (struct sockaddr_in *)&route.rt_gateway;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = gateway.s_addr;

    addr = (struct sockaddr_in*) &route.rt_dst;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = ip.s_addr;

    addr = (struct sockaddr_in*) &route.rt_genmask;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = ip.s_addr;

    // TODO Add the interface name to the request
    route.rt_flags = RTF_UP | RTF_GATEWAY | flags;
    route.rt_metric = 0;

    r = ioctl(fd, op ? SIOCADDRT : SIOCDELRT, &route);

    closesocket(fd);

    return r;
}
#endif

#ifdef __APPLE__

#define ROUNDUP(a) \
    ((a) > 0 ? (1 + (((a) - 1) | (sizeof(long) - 1))) : sizeof(long))

static int GetNetworkGateway(struct in_addr *ip, char *ifName, int ifNameLen)
{
    int ret = -1;
    size_t needed;
    char *buf, *next, *lim;
    struct rt_msghdr *rt;

    int mib[] = {CTL_NET, PF_ROUTE, 0, AF_INET,
        NET_RT_FLAGS, RTF_GATEWAY};

    if (sysctl(mib, 6, NULL, &needed, NULL, 0) < 0) {
	syslog(LOG_ERR, "%s: sysctl net.route.0.0.dump estimate", __func__);
	return -1;
    }

    if ((buf = (char *)malloc(needed)) == NULL) {
	syslog(LOG_ERR, "%s: malloc(%lu)", __func__, (unsigned long)needed);
        return -1;
    }
    if (sysctl(mib, 6, buf, &needed, NULL, 0) < 0) {
	free(buf);
	syslog(LOG_ERR, "%s: sysctl net.route.0.0.dump", __func__);
	return -1;
    }
    lim  = buf + needed;

    for (next = buf; next < lim; next += rt->rtm_msglen) {
	int		i;
	struct sockaddr *sa;
	struct sockaddr *sa_tab[RTAX_MAX];

	rt = (struct rt_msghdr *)next;
	sa = (struct sockaddr *)(rt + 1);
	for (i = 0; i < RTAX_MAX; i++) {
	    if (rt->rtm_addrs & (1 << i)) {
		sa_tab[i] = sa;
		sa = (struct sockaddr *)((char *)sa + ROUNDUP(sa->sa_len));
	    } else {
		sa_tab[i] = NULL;
	    }
	}
	if (((rt->rtm_addrs & (RTA_DST | RTA_GATEWAY)) == (RTA_DST | RTA_GATEWAY))
	    && sa_tab[RTAX_DST]->sa_family == AF_INET
	    && sa_tab[RTAX_GATEWAY]->sa_family == AF_INET) {
	    if (((struct sockaddr_in *)sa_tab[RTAX_DST])->sin_addr.s_addr == 0) {
		char tmpIfName[IFNAMSIZ];
		ip->s_addr = ((struct sockaddr_in *)(sa_tab[RTAX_GATEWAY]))->sin_addr.s_addr;
		if_indextoname(rt->rtm_index, tmpIfName);
		strncpy(ifName, tmpIfName, ifNameLen);
		ret = 0;
	    }
	}
    }

    free(buf);

    return ret;
}

static int SetNetworkRoute(int op, struct in_addr ip, struct in_addr netmask, struct in_addr gateway, char *ifName, int flags)
{
    char sIp[MAXHOSTNAMELEN];
    char sNetmask[MAXHOSTNAMELEN];
    char sGateway[MAXHOSTNAMELEN];
    char cmd[1024];
    inet_ntop(AF_INET, &ip.s_addr, sIp, sizeof(sIp) - 1);
    inet_ntop(AF_INET, &netmask.s_addr, sNetmask, sizeof(sNetmask) - 1);
    inet_ntop(AF_INET, &gateway.s_addr, sGateway, sizeof(sGateway) - 1);

    snprintf(cmd, sizeof(cmd), "/sbin/route %s -net %s %s %s", (op == ROUTE_ADD) ? "add" : "delete", sIp, sGateway, sNetmask);

    FILE *p = popen(cmd, "r");
    if (p) {
	char buf[1024];
	while (fgets(buf, sizeof(buf), p)) {
	    syslog(LOG_INFO, "%s: %s", __func__, buf);
	}
	return pclose(p);
    }
    return -1;
}
#endif

#ifdef _WIN32

#include <iphlpapi.h>

static MIB_IPFORWARDROW nodeRoute;
static int nodeRouteInited = 0;

/**
 * @brief Set network route
 * @param op Operation
 * @param ifName Interface name
 * @param ip Destination IP
 * @param netmasn Destination mask
 * @param gateway Gateway
 * @return Status
 */
static int SetNetworkRoute(int op, struct in_addr ip, struct in_addr netmask, struct in_addr gateway, char *ifName, int flags)
{
    if (op == ROUTE_ADD) {
	if (GetBestRoute(ip.s_addr, 0, &nodeRoute) != NO_ERROR) {
	    syslog(LOG_ERR, "%s: can't get route", __func__);
	    return -1;
	}
	nodeRoute.dwForwardDest = ip.s_addr;
	nodeRoute.dwForwardMask = netmask.s_addr;

	if (CreateIpForwardEntry(&nodeRoute) != NO_ERROR) {
	    syslog(LOG_ERR, "%s: can't add route", __func__);
	    return -1;
	}
	nodeRouteInited = 1;
    } else if (nodeRouteInited && op == ROUTE_DEL) {
	if (DeleteIpForwardEntry(&nodeRoute) != NO_ERROR) {
	    syslog(LOG_ERR, "%s: can't delete route", __func__);
	}
	nodeRouteInited = 0;
    }

    return 0;
}
#endif

/**
 * @brief Set default gateway
 * @param op Operation
 * @param gateway Gateway
 * @param ifName Interface name
 * @return Status
 */
static int SetDefaultGateway(int op, struct in_addr gateway, char *ifName)
{
    int r;
    struct in_addr ip;
    struct in_addr netmask;

    ip.s_addr = 0;
    netmask.s_addr = htonl(0x80000000);

    r = SetNetworkRoute(op, ip, netmask, gateway, ifName, 0);
    if (r == -1) {
	return r;
    }

    ip.s_addr = htonl(0x80000000);

    return SetNetworkRoute(op, ip, netmask, gateway, ifName, 0);
}

#ifdef __linux__

/**
 * @brief Configure client's TUN interface
 * @param ifName TUN interface name
 * @param ip IP
 * @param netmask Netmask
 * @param dstip Destination IP
 * @return Status
 */
static int SetInterfaceAddress(char *ifName, struct in_addr ip, struct in_addr netmask, struct in_addr dstip)
{
    struct ifreq ifr;
    struct sockaddr_in sai;
    int fd;			/* socket fd we use to manipulate stuff with */
    size_t if_name_len;

    memset(&ifr, 0, sizeof(ifr));

    if_name_len = strlen(ifName);
    if (if_name_len < sizeof(ifr.ifr_name)) {
	memcpy(ifr.ifr_name, ifName, if_name_len);
	ifr.ifr_name[if_name_len] = 0;
    } else {
	syslog(LOG_ERR, "%s: if_name too long", __func__);
	return -1;
    }

    /* Create a channel to the NET kernel. */
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd == -1) {
	syslog(LOG_ERR, "%s: Error (%s)", __func__, strerror(errno));
	return -1;
    }

    memset(&sai, 0, sizeof(struct sockaddr));
    sai.sin_family = AF_INET;
    sai.sin_port = 0;

    sai.sin_addr.s_addr = ip.s_addr;
    memcpy((char *)&ifr.ifr_addr, (char *)&sai, sizeof(struct sockaddr));
    if (ioctl(fd, SIOCSIFADDR, &ifr) == -1) {
	syslog(LOG_ERR, "%s: Error (%s)", __func__, strerror(errno));
	closesocket(fd);
	return -1;
    }

    sai.sin_addr.s_addr = netmask.s_addr;
    memcpy((char *)&ifr.ifr_addr, (char *)&sai, sizeof(struct sockaddr));
    if (ioctl(fd, SIOCSIFNETMASK, &ifr) == -1) {
	syslog(LOG_ERR, "%s: Error (%s)", __func__, strerror(errno));
	closesocket(fd);
	return -1;
    }

    sai.sin_addr.s_addr = dstip.s_addr;
    memcpy((char *)&ifr.ifr_addr, (char *)&sai, sizeof(struct sockaddr));
    if (ioctl(fd, SIOCSIFDSTADDR, &ifr) == -1) {
	syslog(LOG_ERR, "%s: Error (%s)", __func__, strerror(errno));
	closesocket(fd);
	return -1;
    }

    if (ioctl(fd, SIOCGIFFLAGS, &ifr) == -1) {
	syslog(LOG_ERR, "%s: Error (%s)", __func__, strerror(errno));
	closesocket(fd);
	return -1;
    }

    ifr.ifr_flags |= IFF_UP | IFF_RUNNING;
    /* ifr.ifr_flags &= ~selector;  // unset something */

    if (ioctl(fd, SIOCSIFFLAGS, &ifr) == -1) {
	syslog(LOG_ERR, "%s: Error (%s)", __func__, strerror(errno));
	closesocket(fd);
	return -1;
    }

    closesocket(fd);

#ifndef DISABLE_IPV6
{
    struct in6_ifreq ifr6;

    fd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_IP);
    if (fd == -1) {
	fprintf(stderr, "%s(%d): Error (%s)\n", __func__, __LINE__, strerror(errno));
    }

    strncpy(ifr.ifr_name, ifName, sizeof(ifr.ifr_name));
    if (ioctl(fd, SIOGIFINDEX, &ifr) == -1) {
	fprintf(stderr, "%s(%d): Error (%s)\n", __func__, __LINE__, strerror(errno));
    }

    ifr6.ifr6_addr.s6_addr[0] = 0xfd;
    ifr6.ifr6_addr.s6_addr[1] = 0x00;
    ifr6.ifr6_addr.s6_addr[2] = 0xde;
    ifr6.ifr6_addr.s6_addr[3] = 0xad;
    ifr6.ifr6_addr.s6_addr[4] = 0xbe;
    ifr6.ifr6_addr.s6_addr[5] = 0xaf;
    ifr6.ifr6_addr.s6_addr[6] = 0x00;
    ifr6.ifr6_addr.s6_addr[7] = 0x00;

    memset((char *)&ifr6.ifr6_addr.s6_addr[8], 0, 4);
    memcpy((char *)&ifr6.ifr6_addr.s6_addr[12], (char *)&ip.s_addr, sizeof(ip.s_addr));

    ifr6.ifr6_ifindex = ifr.ifr_ifindex;
    ifr6.ifr6_prefixlen = 64;
    if (ioctl(fd, SIOCSIFADDR, &ifr6) == -1) {
	fprintf(stderr, "%s(%d): Error (%s)\n", __func__, __LINE__, strerror(errno));
    }

    close(fd);

}
#endif

    return 0;
}
#endif

#ifdef __APPLE__
/**
 * @brief Configure client's TUN interface
 * @param ifName TUN interface name
 * @param ip IP
 * @param netmask Netmask
 * @param dstip Destination IP
 * @return Status
 */
static int SetInterfaceAddress(char *ifName, struct in_addr ip, struct in_addr netmask, struct in_addr dstip)
{
    char sIp[MAXHOSTNAMELEN];
    char sNetmask[MAXHOSTNAMELEN];
    char sDstIp[MAXHOSTNAMELEN];
    char cmd[1024];
    inet_ntop(AF_INET, &ip.s_addr, sIp, sizeof(sIp) - 1);
    inet_ntop(AF_INET, &netmask.s_addr, sNetmask, sizeof(sNetmask) - 1);
    inet_ntop(AF_INET, &dstip.s_addr, sDstIp, sizeof(sDstIp) - 1);
    snprintf(cmd, sizeof(cmd), "/sbin/ifconfig %s %s %s netmask %s", ifName, sIp, sDstIp, sNetmask);

    FILE *p = popen(cmd, "r");
    if (p) {
	char buf[1024];
	while (fgets(buf, sizeof(buf), p)) {
	    syslog(LOG_INFO, "%s: %s", __func__, buf);
	}
	return pclose(p);
    }
    return -1;
}
#endif

/**
 * @brief Setup client interface and routing
 * @param conn Connection
 * @return Status
 */
static int SetupInterfaceAndRouting(char *tunIfName, struct in_addr vpnIp, struct in_addr vpnNetmask, struct in_addr vpnGateway, struct in_addr vpnDns[4], struct in_addr remoteIp)
{
    int r;

    if ((r = SetInterfaceAddress(tunIfName, vpnIp, vpnNetmask, vpnGateway)) == -1) {
	syslog(LOG_ERR, "Cannot set interface address");
    }
#ifdef _WIN32
    else {
	if (!serverHostRoute) {
	    serverHostRoute = 1;

	    /*if (defaultRoute)*/ {
		if (IsCurrentUserLocalAdministrator()) {
		    struct in_addr netmask;
		    netmask.s_addr = 0xFFFFFFFF;

		    if ((r = SetNetworkRoute(ROUTE_ADD, remoteIp, netmask, hostGateway, hostGatewayIfName, RTF_HOST)) == -1) {
			syslog(LOG_ERR, "Cannot add network route for server");
		    }
		} else {
		    syslog(LOG_INFO, "No routing changes, using TAP");
		}
		SetupTunGatewayAndDns(tunIfName, vpnGateway, vpnDns);
	    }
	}

	SetupTunStatus(tunIfName, TRUE);
    }
#else
    else if ((r = GetNetworkGateway(&hostGateway, hostGatewayIfName, sizeof(hostGatewayIfName))) != -1) {
	if (!serverHostRoute) {
	    struct in_addr netmask;
	    syslog(LOG_INFO, "Host device : %s", hostGatewayIfName);
	    syslog(LOG_INFO, "Host gateway: %s", inet_ntoa(hostGateway));
	    netmask.s_addr = 0xFFFFFFFF;

	    serverHostRoute = 1;

	    /*if (defaultRoute)*/ {
		if ((r = SetNetworkRoute(ROUTE_ADD, remoteIp, netmask, hostGateway, hostGatewayIfName, RTF_HOST)) == -1) {
		    syslog(LOG_ERR, "Cannot add network route for server");
		}
		/* else */
		{
		    if ((r = SetDefaultGateway(ROUTE_ADD, vpnGateway, tunIfName)) == -1) {
			syslog(LOG_ERR, "Cannot setup default route via VPN");
		    }
		}
	    }
	}
    }
#endif
    return r;
}

/**
 * @brief Remove client interface and routing
 * @param conn Connection
 * @return Status
 */
static int RemoveInterfaceAndRouting(struct in_addr remoteIp)
{
    int r = 0;
    if (serverHostRoute) {
	serverHostRoute = 0;
	/*if (defaultRoute)*/ {
	    if (IsCurrentUserLocalAdministrator()) {
		struct in_addr netmask;
		netmask.s_addr = 0xFFFFFFFF;

		if ((r = SetNetworkRoute(ROUTE_DEL, remoteIp, netmask, hostGateway, hostGatewayIfName, RTF_HOST)) == -1) {
		    syslog(LOG_ERR, "Cannot delete network route for server");
		}
	    } else {
		syslog(LOG_INFO, "No routing changes, using TAP");
	    }
	    /* else */
	    {
		if ((r = SetDefaultGateway(ROUTE_DEL, vpnGateway, tunIfName)) == -1) {
			syslog(LOG_ERR, "Cannot remove default route via VPN");
		}
	    }
	}
    }
    return r;
}

#ifdef _WIN32
//-------------------------------------------------------------------------
// This function checks the token of the calling thread to see if the caller
// belongs to the Administrators group.
//
// Return Value:
//   TRUE if the caller is an administrator on the local machine.
//   Otherwise, FALSE.
// --------------------------------------------------------------------------
// Based on code from KB #Q118626, at http://support.microsoft.com/kb/118626
BOOL IsCurrentUserLocalAdministrator()
{
    BOOL   fReturn         = FALSE;
    DWORD  dwStatus        = 0;
    DWORD  dwAccessMask    = 0;
    DWORD  dwAccessDesired = 0;
    DWORD  dwACLSize       = 0;
    DWORD  dwStructureSize = sizeof(PRIVILEGE_SET);
    PACL   pACL            = NULL;
    PSID   psidAdmin       = NULL;

    HANDLE hToken              = NULL;
    HANDLE hImpersonationToken = NULL;

    PRIVILEGE_SET   ps = {0};
    GENERIC_MAPPING GenericMapping = {0};

    PSECURITY_DESCRIPTOR     psdAdmin           = NULL;
    SID_IDENTIFIER_AUTHORITY SystemSidAuthority = SECURITY_NT_AUTHORITY;

    // Determine if the current thread is running as a user that is a member 
    // of the local admins group.  To do this, create a security descriptor 
    // that has a DACL which has an ACE that allows only local administrators 
    // access.  Then, call AccessCheck with the current thread's token and
    // the security descriptor.  It will say whether the user could access an
    // object if it had that security descriptor.  Note: you do not need to
    // actually create the object.  Just checking access against the
    // security descriptor alone will be sufficient.

    const DWORD ACCESS_READ  = 1;
    const DWORD ACCESS_WRITE = 2;

        // AccessCheck() requires an impersonation token.  We first get a 
        // primary token and then create a duplicate impersonation token.
        // The impersonation token is not actually assigned to the thread, but
        // is used in the call to AccessCheck.  Thus, this function itself never
        // impersonates, but does use the identity of the thread.  If the thread
        // was impersonating already, this function uses that impersonation 
        // context.
        if (!OpenThreadToken(GetCurrentThread(), TOKEN_DUPLICATE|TOKEN_QUERY, TRUE, &hToken)) {
            if (GetLastError() != ERROR_NO_TOKEN) {
                goto finally;
	    }

            if (!OpenProcessToken(GetCurrentProcess(),
                TOKEN_DUPLICATE|TOKEN_QUERY, &hToken)) {
                goto finally;
	    }
        }

        if (!DuplicateToken (hToken, SecurityImpersonation, &hImpersonationToken)) {
            goto finally;
	}

        // Create the binary representation of the well-known SID that
        // represents the local administrators group.  Then create the 
        // security descriptor and DACL with an ACE that allows only local
        // admins access.  After that, perform the access check.  This will
        // determine whether the current user is a local admin.
        if (!AllocateAndInitializeSid(&SystemSidAuthority, 2,
            SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS,
            0, 0, 0, 0, 0, 0, &psidAdmin)) {
            goto finally;
	}

        psdAdmin = LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH);
        if (psdAdmin == NULL) {
            goto finally;
	}

        if (!InitializeSecurityDescriptor(psdAdmin, SECURITY_DESCRIPTOR_REVISION)) {
            goto finally;
	}

        // Compute size needed for the ACL.
        dwACLSize = sizeof(ACL) + sizeof(ACCESS_ALLOWED_ACE) +
            GetLengthSid(psidAdmin) - sizeof(DWORD);

        pACL = (PACL)LocalAlloc(LPTR, dwACLSize);
        if (pACL == NULL) {
            goto finally;
	}

        if (!InitializeAcl(pACL, dwACLSize, ACL_REVISION2)) {
            goto finally;
	}

        dwAccessMask= ACCESS_READ | ACCESS_WRITE;

        if (!AddAccessAllowedAce(pACL, ACL_REVISION2, dwAccessMask, psidAdmin)) {
            goto finally;
	}

        if (!SetSecurityDescriptorDacl(psdAdmin, TRUE, pACL, FALSE)) {
            goto finally;
	}

        // AccessCheck validates a security descriptor somewhat; set the group
        // and owner so that enough of the security descriptor is filled out to
        // make AccessCheck happy.

        SetSecurityDescriptorGroup(psdAdmin, psidAdmin, FALSE);
        SetSecurityDescriptorOwner(psdAdmin, psidAdmin, FALSE);

        if (!IsValidSecurityDescriptor(psdAdmin)) {
            goto finally;
	}

        dwAccessDesired = ACCESS_READ;

        // Initialize GenericMapping structure even though you
        // do not use generic rights.
        GenericMapping.GenericRead    = ACCESS_READ;
        GenericMapping.GenericWrite   = ACCESS_WRITE;
        GenericMapping.GenericExecute = 0;
        GenericMapping.GenericAll     = ACCESS_READ | ACCESS_WRITE;

        if (!AccessCheck(psdAdmin, hImpersonationToken, dwAccessDesired,
            &GenericMapping, &ps, &dwStructureSize, &dwStatus,
            &fReturn))
        {
            fReturn = FALSE;
        }

finally:
        // Clean up.
        if (pACL) {
            LocalFree(pACL);
	}
        if (psdAdmin) {
            LocalFree(psdAdmin);
	}
        if (psidAdmin) {
            FreeSid(psidAdmin);
	}
        if (hImpersonationToken) {
            CloseHandle (hImpersonationToken);
	}
        if (hToken) {
            CloseHandle (hToken);
	}

    return fReturn;
}
#else
int IsCurrentUserLocalAdministrator()
{
    return geteuid() == 0;
}
#endif
