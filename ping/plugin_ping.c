#include <syslog.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <netinet/in.h>
#endif

#include <time.h>
#include "pico_stack.h"
#include "pico_ipv4.h"
#include "pico_icmp4.h"
#include "pico_dev_loop.h"

#include "plugin.h"
#include "pico_dev_lil.h"

#define NUM_PING 10

static struct pico_ip4 ZERO_IP4 = {
    0
};

static int finished = 0;

static const char *Name = "Ping plugin";
static const int Version = (0.1 * 1000);

static LiliTunPlugin *plugin;
static void *connection;

static struct in_addr vpnIp;
static struct in_addr vpnNetmask;
static struct in_addr vpnGateway;
static struct in_addr vpnDns[4];
static struct in_addr remoteIp;

void cb_ping(struct pico_icmp4_stats *s);

STATIC_PLUGIN_FUNCTION int SetupAddress(void *connection, struct in_addr ip, struct in_addr nm)
{
    vpnIp = ip;
    vpnNetmask = nm;
    return 0;
}

STATIC_PLUGIN_FUNCTION int SetupGateway(void *connection, struct in_addr gw)
{
    vpnGateway = gw;
    return 0;
}

STATIC_PLUGIN_FUNCTION int SetupDns(void *connection, struct in_addr dns[4])
{
    int i;
    for (i = 0; i < 4; i++) {
	vpnDns[i] = dns[i];
    }
    return 0;
}

STATIC_PLUGIN_FUNCTION int MainLoop(void *_connection, struct in_addr _remoteIp)
{
    int id;
    struct pico_ip4 ipaddr, netmask, gateway, zero = ZERO_IP4;
    struct pico_device* dev;

    remoteIp = _remoteIp;
    connection = _connection;

    /* initialise the stack. Super important if you don't want ugly stuff like
     * segfaults and such! */
    pico_stack_init();

    dev = pico_lil_create();
    if (!dev) {
        perror("Creating lil");
        return -1;
    }

//    pico_string_to_ipv4("127.0.0.1", &ipaddr.addr);
//    pico_string_to_ipv4("255.0.0.0", &netmask.addr);

    ipaddr.addr = vpnIp.s_addr;
    netmask.addr = vpnNetmask.s_addr;
    gateway.addr = vpnGateway.s_addr;
    pico_ipv4_link_add(dev, ipaddr, netmask);
    pico_ipv4_route_add(zero, zero, gateway, 1, NULL);
    syslog(LOG_INFO, "Lil created");

    syslog(LOG_INFO, "Starting ping");
    id = pico_icmp4_ping("8.8.8.8", NUM_PING, 1000, 10000, 64, cb_ping);

    if (id == -1)
        return -1;

    /* keep running stack ticks to have picoTCP do its network magic. Note that
     * you can do other stuff here as well, or sleep a little. This will impact
     * your network performance, but everything should keep working (provided
     * you don't go overboard with the delays). */
    while (finished != 1 && plugin->IsAlive(connection))
    {
        usleep(100);
        pico_stack_tick();
    }

    pico_device_destroy(dev);

    syslog(LOG_INFO, "finished !");

    return 0;
}

STATIC_PLUGIN_FUNCTION int Exit(void *connection)
{
    return 0;
}

int PluginInit(LiliTunPlugin *plug)
{
    plugin = plug;

    plug->Name = Name;
    plug->Version = &Version;
    plug->SetupAddress = (int (*)(void *connection, struct in_addr ip, struct in_addr nm)) SetupAddress;
    plug->SetupGateway = (int (*)(void *connection, struct in_addr gw)) SetupGateway;
    plug->SetupDns = (int (*)(void *connection, struct in_addr dns[4])) SetupDns;
    plug->MainLoop = (int (*)(void *connection, struct in_addr remoteIp)) MainLoop;
    plug->Exit = (int (*)(void *connection)) Exit;
    return 0;
}

void *PluginEthernetInit(void **EthernetPacketAvailable, void **EthernetPacketRead, void **EthernetPacketWrite)
{
    *EthernetPacketAvailable = plugin->EthernetPacketAvailable;
    *EthernetPacketRead = plugin->EthernetPacketRead;
    *EthernetPacketWrite = plugin->EthernetPacketWrite;

    return connection;
}


#ifdef _WIN32
uint32_t pico_rand(void)
{
    return rand();
}

void pico_rand_feed(uint32_t feed)
{
    srand(feed);
}
#endif

/* gets called when the ping receives a reply, or encounters a problem */
void cb_ping(struct pico_icmp4_stats *s)
{
    char host[30];
    pico_ipv4_to_string(host, s->dst.addr);
    if (s->err == 0) {
        /* if all is well, print some pretty info */
        syslog(LOG_INFO, "%lu bytes from %s: icmp_req=%lu ttl=%lu time=%lu ms", s->size,
                host, s->seq, s->ttl, (long unsigned int)s->time);
        if (s->seq >= NUM_PING)
            finished = 1;
    } else {
        /* if something went wrong, print it and signal we want to stop */
        syslog(LOG_INFO, "PING %lu to %s: Error %d", s->seq, host, s->err);
        finished = 1;
    }
}
