/*********************************************************************
   LiliTun driver for PicoTCP
   Copyright (c) sashz <sashz@pdaXrom.org>, 2019
 *********************************************************************/
#include "pico_device.h"
#include "pico_dev_tun.h"
#include "pico_stack.h"

#include "pico_dev_lil.h"

struct pico_device_lil {
    struct pico_device dev;
    void *connection;
    int (*EthernetPacketAvailable)(void *connection, int delay);
    int (*EthernetPacketRead)     (void *connection, char *buf, int len);
    int (*EthernetPacketWrite)    (void *connection, char *buf, int len);
};

#define TUN_MTU 2048

static int pico_lil_send(struct pico_device *dev, void *buf, int len)
{
    struct pico_device_lil *lil = (struct pico_device_lil *) dev;
    return lil->EthernetPacketWrite(lil->connection, buf, len);
}

static int pico_lil_poll(struct pico_device *dev, int loop_score)
{
    struct pico_device_lil *lil = (struct pico_device_lil *) dev;
    unsigned char buf[TUN_MTU];
    int len = 0;

    while (loop_score > 0) {
        if (!lil->EthernetPacketAvailable(lil->connection, 0)) {
            break;
        }
        len = lil->EthernetPacketRead(lil->connection, (char *)buf, TUN_MTU);
        if (len <= 0) {
            break;
        }
        pico_stack_recv(dev, buf, len); /* this will copy the frame into the stack */
        loop_score--;
    }

    /* return (original_loop_score - amount_of_packets_received) */
    return loop_score;
}

/* Public interface: create/destroy. */

void pico_lil_destroy(struct pico_device *dev)
{
//    struct pico_device_lil *lil = (struct pico_device_lil *) dev;
    
    /* nothing */
}


struct pico_device *pico_lil_create(void)
{
    struct pico_device_lil *lil = PICO_ZALLOC(sizeof(struct pico_device_lil));

    if (!lil)
        return NULL;

    if( 0 != pico_device_init((struct pico_device *)lil, "lil0", NULL)) {
        dbg("Lil init failed.\n");
        pico_lil_destroy((struct pico_device *)lil);
	PICO_FREE(lil);
        return NULL;
    }

    lil->dev.overhead = 0;

    lil->connection = PluginEthernetInit(
	    (void *)&lil->EthernetPacketAvailable,
	    (void *)&lil->EthernetPacketRead,
	    (void *)&lil->EthernetPacketWrite);


    lil->dev.send = pico_lil_send;
    lil->dev.poll = pico_lil_poll;
    lil->dev.destroy = pico_lil_destroy;
    dbg("Device %s created.\n", lil->dev.name);
    return (struct pico_device *)lil;
}
