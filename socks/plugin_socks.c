#include <syslog.h>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#define closesocket close
#endif

#include <time.h>
#include "pico_tree.h"
#include "pico_stack.h"
#include "pico_ipv4.h"
#include "pico_socket.h"

#include "plugin.h"
#include "pico_dev_lil.h"

#define TCP_SIZE	2048

#define S4_REQUEST_GRANTED	0x5a
#define S4_REQUEST_REJECTED	0x5b
#define S4_REQUEST_FAILED_NO_IDENTD	0x5c
#define S4_REQUEST_FAILED_NO_USERID	0x5d

typedef struct {
    uint8_t		ver;
    uint8_t		cmd;
    uint16_t		port;
    struct in_addr	ip;
} __attribute__((packed)) Socks4Req;

typedef struct {
    uint8_t		rsv;
    uint8_t		rep;
    uint16_t		port;
    struct in_addr	ip;
} __attribute__((packed)) Socks4Rep;

typedef struct {
    int			fd;
    struct pico_socket	*psock;
    uint16_t		state;
    uint16_t		counter;
    Socks4Req		req;
    char		var[256];
    char		name[256];
} SocksClient;

enum {
    CLIENT_GET_REQ = 0,
    CLIENT_GET_VAR,
    CLIENT_GET_NAME,
    CLIENT_RESOLV,
    CLIENT_CONNECT,
    CLIENT_CONNECTED,
    CLIENT_BIND,
    CLIENT_BINDED
};

static struct pico_ip4 ZERO_IP4 = {
    0
};

static int finished = 0;

static const char *Name = "Socks proxy plugin";
static const int Version = (0.1 * 1000);

static LiliTunPlugin *plugin;
static void *connection;

static struct in_addr vpnIp;
static struct in_addr vpnNetmask;
static struct in_addr vpnGateway;
static struct in_addr vpnDns[4];
static struct in_addr remoteIp;

static int SelectTimeout(int fd, int timeout);
static int cread(int fd, char *buf, int n);
static int cwrite(int fd, char *buf, int n);
static void tcp_callback(uint16_t ev, struct pico_socket *s);

static int32_t compareClients(void *ka, void *kb)
{
    return ((SocksClient *)ka)->fd - ((SocksClient *)kb)->fd;
}

PICO_TREE_DECLARE(clientsList, compareClients);

STATIC_PLUGIN_FUNCTION int SetupAddress(void *connection, struct in_addr ip, struct in_addr nm)
{
    vpnIp = ip;
    vpnNetmask = nm;
    return 0;
}

STATIC_PLUGIN_FUNCTION int SetupGateway(void *connection, struct in_addr gw)
{
    vpnGateway = gw;
    return 0;
}

STATIC_PLUGIN_FUNCTION int SetupDns(void *connection, struct in_addr dns[4])
{
    int i;
    for (i = 0; i < 4; i++) {
	vpnDns[i] = dns[i];
    }
    return 0;
}

STATIC_PLUGIN_FUNCTION int MainLoop(void *_connection, struct in_addr _remoteIp)
{
    int status = -1;
    struct pico_device* dev;
    int socksFd, optval;
    struct sockaddr_in local;
    int port = 1080;

    remoteIp = _remoteIp;
    connection = _connection;

    /* initialise the stack. Super important if you don't want ugly stuff like
     * segfaults and such! */
    pico_stack_init();

    dev = pico_lil_create();
    if (!dev) {
        perror("Creating lil");
        goto out;
    } else {
	struct pico_ip4 ipaddr, netmask, gateway, zero = ZERO_IP4;

	ipaddr.addr = vpnIp.s_addr;
	netmask.addr = vpnNetmask.s_addr;
	gateway.addr = vpnGateway.s_addr;
	pico_ipv4_link_add(dev, ipaddr, netmask);
	pico_ipv4_route_add(zero, zero, gateway, 1, NULL);
	syslog(LOG_INFO, "Lil created");
    }

    /* create socks server socket */

    if ((socksFd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	syslog(LOG_ERR, "socks: Cannot create socket (%s)", strerror(errno));
	goto out;
    }

    /* avoid EADDRINUSE error on bind() */
    optval = 1;
    if (setsockopt(socksFd, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval)) < 0) {
	syslog(LOG_ERR, "socks: Cannot set socket options (%s)", strerror(errno));
	closesocket(socksFd);
	goto out;
    }

    memset(&local, 0, sizeof(local));
    local.sin_family = AF_INET;
    local.sin_addr.s_addr = htonl(INADDR_ANY);
    local.sin_port = htons(port);
    if (bind(socksFd, (struct sockaddr *)&local, sizeof(local)) < 0) {
	syslog(LOG_ERR, "socks: Bind error (%s)", strerror(errno));
	closesocket(socksFd);
	goto out;
    }

    if (listen(socksFd, 5) < 0) {
	syslog(LOG_ERR, "socks: Listen error (%s)", strerror(errno));
	closesocket(socksFd);
	goto out;
    }


    status = 0;

    /* keep running stack ticks to have picoTCP do its network magic. Note that
     * you can do other stuff here as well, or sleep a little. This will impact
     * your network performance, but everything should keep working (provided
     * you don't go overboard with the delays). */
    while (finished != 1 && plugin->IsAlive(connection))
    {
	int netFd;
	socklen_t remotelen;
	struct sockaddr_in remote;
	int ret;

	/* wait for connection request */
	if ((ret = SelectTimeout(socksFd, 0)) < 0) {
	    syslog(LOG_INFO, "error (%s)", strerror(errno));
	    status = -1;
	    break;
	}

	if (ret > 0) {
	    SocksClient *socksClient;
	    remotelen = sizeof(remote);
	    memset(&remote, 0, remotelen);
	    if ((netFd = accept(socksFd, (struct sockaddr *)&remote, &remotelen)) < 0) {
		syslog(LOG_ERR, "Accept error (%s)", strerror(errno));
		break;
	    }

	    syslog(LOG_INFO, "Client connected from %s", inet_ntoa(remote.sin_addr));

	    socksClient = PICO_ZALLOC(sizeof(SocksClient));
	    socksClient->fd = netFd;
	    socksClient->psock = NULL;
	    socksClient->state = CLIENT_GET_REQ;
	    socksClient->counter = 0;
	    socksClient->var[0] = 0;
	    socksClient->name[0] = 0;

	    if (pico_tree_insert(&clientsList, socksClient)) {
		/* already in */
		PICO_FREE(socksClient);
		syslog(LOG_ERR, "Client already in list");
	    }
	}

	struct pico_tree_node *index, *tmp;

	pico_tree_foreach_safe(index, &clientsList, tmp) {
	    if (((SocksClient *)index->keyValue)->state < CLIENT_RESOLV) {
		SocksClient *client = (SocksClient *)index->keyValue;
		if ((ret = SelectTimeout(client->fd, 0)) < 0) {
		    closesocket(client->fd);
		    pico_tree_delete(&clientsList, client);
		    syslog(LOG_INFO, "error, remove client");
		    continue;
		} else if (ret == 0) {
		    continue;
		}
		if (client->state == CLIENT_GET_REQ) {
		    int need = sizeof(Socks4Req) - client->counter;
		    char *ptr = (char *) &client->req;
		    ptr += client->counter;
		    ret = cread(client->fd, ptr, need);
		    if (ret <= 0) {
			closesocket(client->fd);
			pico_tree_delete(&clientsList, client);
			syslog(LOG_ERR, "can't read request");
			continue;
		    }
		    if (need == ret) {
			client->state = CLIENT_GET_VAR;
			client->counter = 0;

		    } else {
			if (!client->counter) {
			    if (client->req.ver != 4) {
				closesocket(client->fd);
				pico_tree_delete(&clientsList, client);
				syslog(LOG_ERR, "Only Socks4 and Socks4a are supported!");
				continue;
			    }
			}
			client->counter += ret;
		    }
		} else if (client->state == CLIENT_GET_VAR || client->state == CLIENT_GET_NAME) {
		    int need = 1;
		    char *ptr = (client->state == CLIENT_GET_VAR)?&client->var[client->counter]:&client->name[client->counter];
		    ret = cread(client->fd, ptr, need);
		    if (ret <= 0) {
			closesocket(client->fd);
			pico_tree_delete(&clientsList, client);
			syslog(LOG_ERR, "can't read request");
			continue;
		    }
		    if (ptr[0] == 0) {
			if (client->state == CLIENT_GET_VAR && client->req.ip.s_addr == 0x01000000) {
			    client->state = CLIENT_GET_NAME;
			    client->counter = 0;
			} else {
			    if (client->req.ip.s_addr == 0x01000000) {
				syslog(LOG_INFO, "Client (%s) socks4a requested %s to %s:%d",
					client->var, (client->req.cmd == 1)?"CONNECT":"BIND", client->name, ntohs(client->req.port));
				client->state = CLIENT_RESOLV;
			    } else {
				syslog(LOG_INFO, "Client (%s) socks4 requested %s to %s:%d",
					client->var, (client->req.cmd == 1)?"CONNECT":"BIND", inet_ntoa(client->req.ip), ntohs(client->req.port));
				client->state = CLIENT_CONNECT;

				if (!(client->psock = pico_socket_open(PICO_PROTO_IPV4, PICO_PROTO_TCP, &tcp_callback))) {
				    syslog(LOG_INFO, "error pico_socket_open()");
				    closesocket(client->fd);
				    pico_tree_delete(&clientsList, client);
				    continue;
				}

    	uint32_t val = 60000;
        pico_socket_setoption(client->psock, PICO_SOCKET_OPT_KEEPIDLE, &val);
        pico_socket_setoption(client->psock, PICO_SOCKET_OPT_KEEPINTVL, &val);
        val = 7;
        pico_socket_setoption(client->psock, PICO_SOCKET_OPT_KEEPCNT, &val);

				struct pico_ip4 ipaddr;
				ipaddr.addr = client->req.ip.s_addr;

				if (pico_socket_connect(client->psock, &ipaddr, client->req.port) < 0) {
				    syslog(LOG_INFO, "error pico_socket_connect()");
				    pico_socket_close(client->psock);
				    closesocket(client->fd);
				    pico_tree_delete(&clientsList, client);
				    continue;
				}

			    }

			    if (client->req.cmd != 1) {
				closesocket(client->fd);
				pico_tree_delete(&clientsList, client);
				syslog(LOG_ERR, "Only CONNECT supported");
				continue;
			    }
			}
		    } else {
			client->counter += ret;
			if (client->counter > 255) {
			    closesocket(client->fd);
			    pico_tree_delete(&clientsList, client);
			    syslog(LOG_ERR, "string too long, reject client");
			}
		    }
		}
	    }
	}

//	for (int z = 0; z < 100; z++) {
        usleep(100);
        pico_stack_tick();
//	}
    }

    {
	struct pico_tree_node *index, *tmp;
	pico_tree_foreach_safe(index, &clientsList, tmp) {
	    SocksClient *client = (SocksClient *)index->keyValue;
	    if (client->psock) {
#warning FIXME
	    }
	    closesocket(client->fd);
	    pico_tree_delete(&clientsList, client);
	}
    }

    closesocket(socksFd);

out:

    if (dev) {
	pico_device_destroy(dev);
    }

    syslog(LOG_INFO, "finished !");

    return status;
}

STATIC_PLUGIN_FUNCTION int Exit(void *connection)
{
    return 0;
}

int PluginInit(LiliTunPlugin *plug)
{
    plugin = plug;

    plug->Name = Name;
    plug->Version = &Version;
    plug->SetupAddress = (int (*)(void *connection, struct in_addr ip, struct in_addr nm)) SetupAddress;
    plug->SetupGateway = (int (*)(void *connection, struct in_addr gw)) SetupGateway;
    plug->SetupDns = (int (*)(void *connection, struct in_addr dns[4])) SetupDns;
    plug->MainLoop = (int (*)(void *connection, struct in_addr remoteIp)) MainLoop;
    plug->Exit = (int (*)(void *connection)) Exit;
    return 0;
}

void *PluginEthernetInit(void **EthernetPacketAvailable, void **EthernetPacketRead, void **EthernetPacketWrite)
{
    *EthernetPacketAvailable = plugin->EthernetPacketAvailable;
    *EthernetPacketRead = plugin->EthernetPacketRead;
    *EthernetPacketWrite = plugin->EthernetPacketWrite;

    return connection;
}

#ifdef _WIN32
uint32_t pico_rand(void)
{
    return rand();
}

void pico_rand_feed(uint32_t feed)
{
    srand(feed);
}
#endif

/**
 * @brief Wait on socket with timeout
 * @param fd Socket
 * @param timeout Timeout
 * @return Data available
 */
static int SelectTimeout(int fd, int timeout)
{
    fd_set rfds;
    struct timeval tv;

    tv.tv_sec = timeout;
    tv.tv_usec = 10;

    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);

    return select(fd + 1, &rfds, NULL, NULL, &tv);
}

/**
 * @brief Read from socket
 * @param fd Socket
 * @param buf Buffer
 * @param n Buffer size
 * @return Number of bytes actually read
 */
static int cread(int fd, char *buf, int n)
{

    int nread;

    if ((nread = recv(fd, buf, n, 0)) < 0) {
	syslog(LOG_ERR, "%s: Reading data (%s)", __func__, strerror(errno));
    }
    return nread;
}

/**
 * @brief Write to socket
 * @param fd Socket
 * @param buf Buffer
 * @param n Buffer size
 * @return Number of bytes actually write
 */
static int cwrite(int fd, char *buf, int n)
{

    int nwrite;

    if ((nwrite = send(fd, buf, n, 0)) < 0) {
	syslog(LOG_ERR, "%s: Writing data (%s)", __func__, strerror(errno));
    }
    return nwrite;
}

static int ReadBytesFromFd(int fd, void *data, int n)
{
    int total = 0;
    char *buf = data;

    do {
	int ret = cread(fd, buf, n);
	if (ret < 0) {
	    return -1;
	} else if (ret == 0) {
	    break;
	}
	total += ret;
	n -= ret;
	buf += ret;
    } while (n > 0);

    return total;
}

static int WriteBytesToFd(int fd, void *data, int n)
{
    int total = 0;
    char *buf = data;

    do {
	int ret = cwrite(fd, buf, n);
	if (ret < 0) {
	    return -1;
	} else if (ret == 0) {
	    break;
	}
	total += ret;
	n -= ret;
	buf += ret;
    } while (n > 0);

    return total;
}

static char *ReadStringFromFd(int fd, char *buf, int n)
{
    char *ptr = buf;

    while (n > 0) {
	int ret = ReadBytesFromFd(fd, ptr, 1);
	if (ret < 1) {
	    return NULL;
	} else if (ptr[0] == 0) {
	    return buf;
	}
	ptr++;
	n--;
    }

    return buf;
}

static int SendSocks4Reply(int fd, int rep, uint16_t port, struct in_addr ip)
{
    Socks4Rep reply;
    reply.rsv = 0;
    reply.rep = rep;
    reply.port = port;
    reply.ip = ip;

    if (WriteBytesToFd(fd, &reply, sizeof(Socks4Rep)) < sizeof(Socks4Rep)) {
	syslog(LOG_INFO, "Can't send reply");
	return -1;
    }

    return 0;
}

static void tcp_callback(uint16_t ev, struct pico_socket *s)
{
    struct pico_tree_node *index;
    SocksClient *client = NULL;

    syslog(LOG_INFO, "tcp_callback");

    pico_tree_foreach(index, &clientsList) {
	if (((SocksClient *)index->keyValue)->psock == s) {
	    client = (SocksClient *)index->keyValue;
	    break;
	}
    }

    if (!client || client->state < CLIENT_CONNECT) {
	syslog(LOG_INFO, "%s: socket not found", __func__);
	return;
    }

    if (ev & PICO_SOCK_EV_CONN) {
	client->state = CLIENT_CONNECTED;
	syslog(LOG_INFO, "Connection established with server");
	SendSocks4Reply(client->fd, S4_REQUEST_GRANTED, client->req.port, client->req.ip);
    }

    if (ev & PICO_SOCK_EV_FIN) {
	syslog(LOG_INFO, "Socket closed");
//	pico_socket_close(s);
	pico_socket_del(s);
	closesocket(client->fd);
	pico_tree_delete(&clientsList, client);
	return;
    }

    if (ev & PICO_SOCK_EV_ERR) {
        syslog(LOG_INFO, "Socket error received: %s. Bailing out.", strerror(pico_err));
	pico_socket_close(s);
	closesocket(client->fd);
	pico_tree_delete(&clientsList, client);
	return;
    }

    if (ev & PICO_SOCK_EV_CLOSE) {
        printf("Socket received close from peer - Wrong case if not all client data sent!\n");
	pico_socket_close(s);
	closesocket(client->fd);
	pico_tree_delete(&clientsList, client);
        return;
    }

    if (ev & PICO_SOCK_EV_WR) {
syslog(LOG_INFO, "cb write");
	char buffer[TCP_SIZE];
	int ret;
	if ((ret = SelectTimeout(client->fd, 0)) > 0) {
	    if ((ret = cread(client->fd, buffer, sizeof(buffer))) > 0) {
		if (pico_socket_write(s, buffer, ret) != ret) {
		    syslog(LOG_ERR, "error pico_socket_write()");
		}
	    } else {
		syslog(LOG_ERR, "client connection closed");
		pico_socket_close(s);
		//closesocket(client->fd);
		//pico_tree_delete(&clientsList, client);
	    }
	}
	if (ret < 0) {
	    syslog(LOG_ERR, "Error read from client (select)");
	}
    }

    if (ev & PICO_SOCK_EV_RD) {
syslog(LOG_INFO, "cb read");
	char buffer[TCP_SIZE];
	int ret;
	do {
	    ret = pico_socket_read(s, buffer, sizeof(buffer));
	    if (ret > 0) {
		if (cwrite(client->fd, buffer, ret) != ret) {
		    syslog(LOG_ERR, "error send data to client");
		    pico_socket_close(s);
		    //closesocket(client->fd);
		    //pico_tree_delete(&clientsList, client);
		    break;
		}
	    } else if (ret < 0) {
		syslog(LOG_ERR, "error pico_socket_read()");
		break;
	    }
	} while (ret > 0);
    }
}
