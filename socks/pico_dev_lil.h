/*********************************************************************
   LiliTun driver for PicoTCP
   Copyright (c) sashz <sashz@pdaXrom.org>, 2019
 *********************************************************************/
#ifndef __PICO_DEV_LIL__
#define __PICO_DEV_LIL__
#include "pico_config.h"
#include "pico_device.h"

//void *PluginEthernetInit(int (*EthernetPacketAvailable)(void, int), void **EthernetPacketRead, void **EthernetPacketWrite);
void *PluginEthernetInit(void **EthernetPacketAvailable, void **EthernetPacketRead, void **EthernetPacketWrite);

void pico_lil_destroy(struct pico_device *tun);
struct pico_device *pico_lil_create(void);

#endif
