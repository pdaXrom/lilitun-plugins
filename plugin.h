#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#ifndef STATIC_PLUGIN_FUNCTION
#define STATIC_PLUGIN_FUNCTION
#endif

typedef struct {
    /* Setting from lilitun app */
    int (*IsAlive)(void *connection);
    int (*EthernetPacketAvailable)(void *connection, int delay);
    int (*EthernetPacketRead)     (void *connection, char *buf, int len);
    int (*EthernetPacketWrite)    (void *connection, char *buf, int len);
    /* Getting from plugin */
    const char *Name;
    const int  *Version;
    int (*SetupAddress)(void *connection, struct in_addr ip, struct in_addr nm);
    int (*SetupGateway)(void *connection, struct in_addr gw);
    int (*SetupDns)    (void *connection, struct in_addr dns[4]);
    int (*MainLoop)    (void *connection, struct in_addr remoteIp);
    int (*Exit)        (void *connection);
    void *handle;
} LiliTunPlugin;

/* User plugin initialization */
int PluginInit(LiliTunPlugin *plug);

/* setup VPN client address */
STATIC_PLUGIN_FUNCTION int SetupAddress(void *connection, struct in_addr ip, struct in_addr nm);

/* setup VPN gateway address */
STATIC_PLUGIN_FUNCTION int SetupGateway(void *connection, struct in_addr gw);

/* setup VPN dns addresses */
STATIC_PLUGIN_FUNCTION int SetupDns(void *connection, struct in_addr dns[4]);

/* VPN main loop */
STATIC_PLUGIN_FUNCTION int MainLoop(void *connection, struct in_addr remoteIp);

/* Close VPN connection */
STATIC_PLUGIN_FUNCTION int Exit(void *connection);

#endif
