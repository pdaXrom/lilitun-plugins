#include <stdio.h>
#include <stdarg.h>
#include <syslog.h>

void openlog (char *ident, int option, int facility)
{
}

void syslog( int pri, const char *fmt, ... )
{
    char message[2048];
    va_list ap;

    va_start( ap, fmt );
    //vsyslog( pri, fmt, ap );

    vsprintf_s(message, sizeof(message), fmt, ap);

    va_end( ap );

    fprintf(stderr, "syslog: %s\n", message);
}
