#ifndef __COMPAT_H__
#define __COMPAT_H__

#ifdef	__cplusplus
extern "C" {
#endif

int setenv(const char *name, const char *value, int overwrite);

char *realpath(const char *path, char *resolved);

const char *winerror(int err);

#ifdef	__cplusplus
}
#endif

#endif
