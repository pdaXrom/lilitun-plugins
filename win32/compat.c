#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include "compat.h"

int setenv(const char *name, const char *value, int overwrite)
{
    char buffer[strlen(name) + strlen(value) + 2];

    snprintf(buffer, sizeof(buffer), "%s=%s", name, value);

    return _putenv(buffer);
}

char *realpath(const char *path, char *resolved)
{
    char *ret;
    char *_ret;

    if (!resolved)
	ret = malloc(_MAX_PATH);
    else
	ret = resolved;

    if (!(_ret = _fullpath(ret, path, _MAX_PATH))) {
	if (!resolved)
	    free(ret);
    } else {
	if (ret[strlen(ret) - 1] == '\\')
	    ret[strlen(ret) - 1] = 0;
    }

    return _ret;
}

const char *winerror(int err)
{
    static char buf[1024], *ptr;

    ptr = buf + sprintf(buf, "(%d) ", err);

    if(!FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
	NULL, err, MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL), ptr, sizeof(buf) - (ptr - buf), NULL)) {
	strcpy(ptr, "(unable to format errormessage)");
    };

    if((ptr = strchr(buf, '\r'))) {
	*ptr = '\0';
    }

	return buf;
}
