#ifndef __SIGNAL_H__
#define __SIGNAL_H__

#define SIGHUP	1
#define SIGINT	2
#define SIGUSR1	10
#define SIGPIPE	13

#define	SIG_IGN	1	/* Ignore signal.  */

#endif
