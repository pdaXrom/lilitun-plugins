#ifdef _WIN32
#include <windows.h>
#else
#include <netinet/in.h>
#endif
#include "plugin.h"

static const char *Name = "TEMPLATE plugin";
static const int Version = (0.1 * 1000);

/* setup VPN client address */
STATIC_PLUGIN_FUNCTION int SetupAddress(void *connection, struct in_addr ip, struct in_addr nm)
{
    return -1;
}

/* setup VPN gateway address */
STATIC_PLUGIN_FUNCTION int SetupGateway(void *connection, struct in_addr gw)
{
    return -1;
}

/* setup VPN dns addresses */
STATIC_PLUGIN_FUNCTION int SetupDns(void *connection, struct in_addr dns[4])
{
    return -1;
}

/* VPN main loop */
STATIC_PLUGIN_FUNCTION int MainLoop(void *connection, struct in_addr remoteIp)
{
    return -1;
}

/* Close VPN connection */
STATIC_PLUGIN_FUNCTION int Exit(void *connection)
{
    return -1;
}

/* User plugin initialization */
int PluginInit(LiliTunPlugin *plug)
{
    plug->Name = Name;
    plug->Version = &Version;
    plug->SetupAddress = (int (*)(void *connection, struct in_addr ip, struct in_addr nm)) SetupAddress;
    plug->SetupGateway = (int (*)(void *connection, struct in_addr gw)) SetupGateway;
    plug->SetupDns = (int (*)(void *connection, struct in_addr dns[4])) SetupDns;
    plug->MainLoop = (int (*)(void *connection, struct in_addr remoteIp)) MainLoop;
    plug->Exit = (int (*)(void *connection)) Exit;
    return 0;
}
